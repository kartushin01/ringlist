﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RingList
{
    class Program
    {


        static void Main(string[] args)
        {

            RingList<string> circularList = new RingList<string>();

            circularList.Add("London");
            circularList.Add("NewYork");
            circularList.Add("Moscow");
            circularList.Add("Paris");
            circularList.Add("Berlin");
            
            
            foreach (var item in circularList)
            {
                Console.WriteLine(item);
            }

            circularList.Remove("Paris");

            
            Console.WriteLine("После удаления:");
            
            
            foreach (var item in circularList)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
